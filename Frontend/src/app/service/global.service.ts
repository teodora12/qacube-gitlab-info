import {Injectable, NgZone} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {ConnectionDTO} from '../model/ConnectionDTO';
import {ConnectionWithCommitsDTO} from '../model/ConnectionWithCommitsDTO';
import {ConnectionWithPullRequestsDTO} from '../model/ConnectionWithPullRequestsDTO';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor(private http: HttpClient, private zone: NgZone) { }

  private refreshNeeded = new Subject<void>();

  get refreshNeeded$(): Subject<void> {
    return this.refreshNeeded;
  }

  createConnectionForApplication(connection: any): Observable<ConnectionDTO> {
    return this.http.post<ConnectionDTO>('/api/connections/create', connection)
      .pipe(
        tap(() => {
          this.refreshNeeded.next();
        })
      );
  }

  getCommits(connectionId: any): Observable<ConnectionWithCommitsDTO>  {
    return this.http.get<ConnectionWithCommitsDTO>('/api/connections/'.concat(connectionId).concat('/commits'))
      .pipe(
        tap(() => {
          this.refreshNeeded.next();
        })
      );
  }

  getPullRequests(connectionId: any): Observable<ConnectionWithPullRequestsDTO> {
    return this.http.get<ConnectionWithPullRequestsDTO>('/api/connections/'.concat(connectionId).concat('/pullRequests'));
  }


  private getEventSource(url: string): EventSource {
    return new EventSource(url);
  }
}
