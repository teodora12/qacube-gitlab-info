import { Component, Input, OnInit} from '@angular/core';
import {ToastrManager} from 'ng6-toastr-notifications';
import { NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {GlobalService} from '../../../service/global.service';
import {ConnectionsService} from '../connections.service';


@Component({
  selector: 'app-connection-modal',
  templateUrl: './connection-modal.component.html',
  styleUrls: ['./connection-modal.component.css'],
  providers: [NgbActiveModal]
})

export class ConnectionModalComponent implements OnInit {

  @Input() updateOrCreate;
  @Input() connection;
  @Input() application;
  @Input() ref;
  @Input() closeRes;

  constructor(private toastr: ToastrManager, private globalService: GlobalService,
              public activeModal: NgbActiveModal, private connService: ConnectionsService) { }

  ngOnInit(): void {

    console.log(this.connection);
    console.log(this.updateOrCreate);
    console.log(this.application);
  }

  createOrUpdateConn(): any {

    if (this.connection.name === '' || this.connection.name === undefined || this.connection.name === null
      || this.connection.branchName === '' || this.connection.branchName === undefined || this.connection.branchName === null
      || this.connection.projectName === '' || this.connection.projectName === null || this.connection.projectName === undefined
      || this.connection.url === '' || this.connection.url === undefined || this.connection.url === null
      || this.connection.applicationDTO.name === '' || this.connection.applicationDTO.name === undefined
      || this.connection.applicationDTO.name === null){
      this.toastr.warningToastr('All fields except access token are mandatory!');
      return;
    }

    const regex = new RegExp( '^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$');

    if (regex.test(this.connection.url) === false) {
      this.toastr.warningToastr('Please type url in format: https://gitlab.com/**username**/**repository_name** ', 'Wrong URL format!');
      return;
    }


    if (this.connection.token === null){
      this.connection.token = '';
    }

    if (this.connection.id === null || this.connection.id === '' || this.connection.id === undefined) {
      this.saveConnection();
    } else {
      this.updateConnection();
    }
  }

  private saveConnection(): void {

    this.globalService.createConnectionForApplication(this.connection).subscribe(res => {
      this.ref.close();
    }, error => {
      this.toastr.errorToastr('Error', 'Error while creating the connection, something went wrong. ' +
        'Invalid token or connection with this name already exists!');
    });

  }

  private updateConnection(): void {

    this.connService.updateConnection(this.connection).subscribe( res => {
      this.ref.close();
    }, error => {
      this.toastr.errorToastr('Error', 'Invalid token.');
    });
    this.connection.id = '';
  }

}
