import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ConnectionModalComponent} from './connection-modal.component';

const routes: Routes = [
  {
    path: '',
    component: ConnectionModalComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ConnectionModalRoutingModule { }
