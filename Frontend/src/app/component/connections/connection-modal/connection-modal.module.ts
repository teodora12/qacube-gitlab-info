import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ConnectionModalRoutingModule} from './connection-modal-routing.module';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    ConnectionModalRoutingModule,
    FormsModule,
    NgbModule
  ],
  declarations: [],
  providers: [NgbActiveModal]

})

export class ConnectionModalModule { }
