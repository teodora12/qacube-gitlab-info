import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConnectionsRoutingModule} from './connections-routing.module';
import {ConnectionsComponent} from './connections.component';
import {FormsModule} from '@angular/forms';
import { ConnectionModalComponent } from './connection-modal/connection-modal.component';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    ConnectionsRoutingModule,
    FormsModule,
    NgbModule
  ],
  declarations: [ConnectionsComponent, ConnectionModalComponent],
  entryComponents: [ConnectionModalComponent]
})

export class ConnectionsModule { }
