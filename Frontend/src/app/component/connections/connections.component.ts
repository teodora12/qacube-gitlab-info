import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ModalDismissReasons, NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {GlobalService} from '../../service/global.service';
import {ConnectionsService} from './connections.service';
import {ConnectionDTO} from '../../model/ConnectionDTO';
import {ApplicationDTO} from '../../model/ApplicationDTO';
import {ApplicationWithConnectionsDTO} from '../../model/ApplicationWithConnectionsDTO';
import {Location} from '@angular/common';
import {ConnectionModalComponent} from './connection-modal/connection-modal.component';

@Component({
  selector: 'app-connections',
  templateUrl: './connections.component.html',
  styleUrls: ['./connections.component.css']
})
export class ConnectionsComponent implements OnInit {

  application: { name: string; id: string;
    connectionDTOS: ConnectionDTO[] } = new ApplicationWithConnectionsDTO();
  connection: { name: string; applicationDTO: ApplicationDTO; branchName: string;
    id: string; projectName: string; url: string; token: string } = new ConnectionDTO();
  applicationId: any;
  closeResult = '';
  modalRef: NgbModalRef;
  clickedId: any;
  updateOrCreate: string;


  constructor(private connectionsServices: ConnectionsService, private route: ActivatedRoute,
              private toastr: ToastrManager, private modalService: NgbModal, private globalService: GlobalService,
              private router: Router, private location: Location) {
    this.application = { id: '', name: '', connectionDTOS : []};
    this.connection = {id: '', name: '', url: '', token: '', applicationDTO: new ApplicationDTO(), projectName: '', branchName: ''};
    this.updateOrCreate = '';
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.applicationId = +params.id; // (+) converts string 'id' to a number
    });
    this.connectionsServices.refreshNeeded$.subscribe(res => {
      this.getConnections();
    });
    this.globalService.refreshNeeded$.subscribe(res => {
      this.getConnections();
    });
    this.getConnections();
  }

  getConnections(): any {
    this.connectionsServices.getConnectionsForApplication(this.applicationId).subscribe((res: any) => {

      this.application = new ApplicationWithConnectionsDTO().deserialize(res.data.getApplicationWithConnections);

      if (this.application.connectionDTOS.length === 0){
        this.toastr.warningToastr('There are no connections for this application yet. Try creating new one!');
      }
      this.clearInputFields();
    }, error => {
      this.toastr.errorToastr('Error');
    });
  }

  openCreate(): any {

    this.updateOrCreate = 'create';

    this.modalRef = this.modalService.open(ConnectionModalComponent);
    this.modalRef.componentInstance.connection = this.connection;
    this.modalRef.componentInstance.updateOrCreate = this.updateOrCreate;
    this.modalRef.componentInstance.ref = this.modalRef;
    this.modalRef.componentInstance.closeRes = this.closeResult;

    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openUpdate(id): void{
    this.updateOrCreate = 'update';
    this.clickedId = id;

    for (const connectionDTO of this.application.connectionDTOS){
      if (connectionDTO.id === this.clickedId) {
        Object.assign(this.connection, connectionDTO);
        console.log(this.connection);
      }
    }
    this.modalRef = this.modalService.open(ConnectionModalComponent);
    this.modalRef.componentInstance.connection = this.connection;
    this.modalRef.componentInstance.updateOrCreate = this.updateOrCreate;
    this.modalRef.componentInstance.application = this.application;
    this.modalRef.componentInstance.ref = this.modalRef;
    this.modalRef.componentInstance.closeRes = this.closeResult;

    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });

  }

  private getDismissReason(reason: any): string {

    this.clearInputFields();
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else if (reason === 'Cross click'){
      return 'by clicking on a x';
    }else if (reason === 'Cancel'){
      return 'by clicking on cancel';
    } else {
      return `with: ${reason}`;
    }
  }

  deleteConnection(id: any): any {

    this.connection.id = id;
    this.connectionsServices.deleteConnection(this.connection).subscribe( res => {
      console.log(res);
    }, error => {
      this.toastr.errorToastr('Error', 'Error while deleting the connection, something went wrong');
    });
  }

  back(): void {
    this.location.back();
  }

  clearInputFields(): void {
    this.clickedId = '';
    this.connection = {id: '', name: '', url: '', token: '', applicationDTO: new ApplicationDTO(), projectName: '', branchName: ''};
    this.connection.applicationDTO.id = this.application.id;
    this.connection.applicationDTO.name = this.application.name;
  }

}
