import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {tap} from 'rxjs/operators';
import {ConnectionDTO} from '../../model/ConnectionDTO';
import {ApplicationWithConnectionsDTO} from '../../model/ApplicationWithConnectionsDTO';

@Injectable({
  providedIn: 'root'
})
export class ConnectionsService {

  constructor(private http: HttpClient) { }

  private refreshNeeded = new Subject<void>();

  get refreshNeeded$(): Subject<void> {
    return this.refreshNeeded;
  }

  getConnectionsForApplication(id: any): Observable<ApplicationWithConnectionsDTO> {
    // return this.http.get<ApplicationWithConnectionsDTO>('/api/connections/get/application/'.concat(id));
    const queryGraphql = '{\n' +
      '   getApplicationWithConnections(id:' + id + ') {\n' +
      '        applicationDTO {\n' +
      '        id\n' +
      '        name }\n' +
      '        connectionDTOS {\n' +
        '        id\n' +
        '        name\n' +
        '        url\n' +
        '        token\n' +
        '        branchName\n' +
        '        projectId\n' +
        '        applicationDTO {\n' +
          '        id\n' +
          '        name }\n' +
        '        projectName }\n' +
      '   }\n' +
      ' }';

    return this.http.post<ApplicationWithConnectionsDTO>('/api/graphql', queryGraphql);

  }

  deleteConnection(connection: any): Observable<ConnectionDTO> {
    return this.http.put<ConnectionDTO>('/api/connections/delete', connection)
      .pipe(
        tap(() => {
          this.refreshNeeded.next();
        })
      );
  }

  updateConnection(connection: any): Observable<ConnectionDTO> {
    return this.http.put<ConnectionDTO>('/api/connections/put', connection)
      .pipe(
        tap(() => {
          this.refreshNeeded.next();
        })
      );
  }

}
