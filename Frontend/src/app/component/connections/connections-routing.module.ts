import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ConnectionsComponent} from './connections.component';
import {ConnectionModalComponent} from './connection-modal/connection-modal.component';

const routes: Routes = [
  {
    path: '',
    component: ConnectionsComponent
  },
  // {
  //   path: '',
  //   component: ConnectionModalComponent
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class ConnectionsRoutingModule { }
