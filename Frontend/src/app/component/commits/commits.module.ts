import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CommitsRoutingModule} from './commits-routing.module';
import {CommitsComponent} from './commits.component';

@NgModule({
  imports: [
    CommonModule,
    CommitsRoutingModule
  ],
  declarations: [CommitsComponent]
})

export class CommitsModule {}
