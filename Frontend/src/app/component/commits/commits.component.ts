import {Component, OnDestroy, OnInit} from '@angular/core';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ActivatedRoute, Router} from '@angular/router';
import {GlobalService} from '../../service/global.service';
import {ConnectionWithCommitsDTO} from '../../model/ConnectionWithCommitsDTO';
import {ApplicationDTO} from '../../model/ApplicationDTO';
import {Location} from '@angular/common';
import {not} from 'rxjs/internal-compatibility';

@Component({
  selector: 'app-commits',
  templateUrl: './commits.component.html',
  styleUrls: ['./commits.component.css']
})
export class CommitsComponent implements OnInit, OnDestroy {

  applicationId: any;
  connectionId: any;
  connectionWithCommitsDTO: { commitDTOS: any[]; projectId: string; name: string; applicationDTO: ApplicationDTO;
    branchName: string; id: string; projectName: string; url: string; token: string } = new ConnectionWithCommitsDTO();
  eventSource: any;

  constructor(private globalService: GlobalService, private toastr: ToastrManager,
              private route: ActivatedRoute, private router: Router, private location: Location) {

    this.connectionWithCommitsDTO = {id: '', url: '', branchName: '', name: '', commitDTOS: [],
      applicationDTO: new ApplicationDTO(), projectName: '', token: '', projectId: ''};

    window.onbeforeunload = e => {
      if (this.eventSource){
        this.eventSource.close();
      }
    };
  }

  ngOnInit(): void {

    this.route.params.subscribe(params => {
      this.applicationId = +params.appId; // (+) converts string 'id' to a number
      this.connectionId = +params.connId;
    });
    this.globalService.getCommits(this.connectionId).subscribe(res => {
      this.connectionWithCommitsDTO = new ConnectionWithCommitsDTO().deserialize(res);
    }, error => {
      this.toastr.errorToastr('Error', 'Invalid token.');
    });

    this.eventSource = new EventSource('http://localhost:8080/api/notifications/subscribe');

    this.eventSource.addEventListener('connectionWithCommitsDTO', event => {
      const notification = JSON.parse(event.data);
      this.connectionWithCommitsDTO = new ConnectionWithCommitsDTO().deserialize(notification);
      console.log(this.connectionWithCommitsDTO);

    });

    // todo ugasiti event source kada se izlazi iz metode da ne bi ovde pristizao connectionWithPullRequestsDTO
    // this.eventSource.onmessage = event => {
    //   const notification = JSON.parse(event.data);
    //   console.log(event.data);
    //   console.log('**************');
    //   console.log(notification);
    //   console.log('/////////////////');
    //   this.connectionWithCommitsDTO = new ConnectionWithCommitsDTO().deserialize(notification);
    //   console.log(this.connectionWithCommitsDTO);
    // };

  }

  back(): void {
    this.location.back();
  }

  ngOnDestroy(): void {
    if (this.eventSource){
      this.eventSource.close();
    }
  }

}
