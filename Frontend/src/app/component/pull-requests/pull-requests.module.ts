import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PullRequestsRoutingModule} from './pull-requests-routing.module';
import {PullRequestsComponent} from './pull-requests.component';


@NgModule({
  imports: [
    CommonModule,
    PullRequestsRoutingModule
  ],
  declarations: [PullRequestsComponent]
})


export class PullRequestsModule {}
