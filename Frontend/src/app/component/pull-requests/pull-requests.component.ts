import {Component, OnDestroy, OnInit} from '@angular/core';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ActivatedRoute, Router} from '@angular/router';
import {GlobalService} from '../../service/global.service';
import {ConnectionWithPullRequestsDTO} from '../../model/ConnectionWithPullRequestsDTO';
import {ApplicationDTO} from '../../model/ApplicationDTO';
import {Location} from '@angular/common';

@Component({
  selector: 'app-pull-requests',
  templateUrl: './pull-requests.component.html',
  styleUrls: ['./pull-requests.component.css']
})
export class PullRequestsComponent implements OnInit, OnDestroy {

  applicationId: any;
  connectionId: any;
  connectionsWithPullRequestsDTO: { projectId: string; name: string; applicationDTO: ApplicationDTO; branchName: string;
    id: string; projectName: string; pullRequestDTOS: any[]; url: string; token: string } = new ConnectionWithPullRequestsDTO();
  eventSource: any;

  constructor(private globalService: GlobalService, private toastr: ToastrManager,
              private route: ActivatedRoute, private router: Router, private location: Location) {

    // this.connectionsWithPullRequestsDTO = { applicationDTO : {}, pullRequestDTOS: [], author: {}, assignee: {}};
    this.connectionsWithPullRequestsDTO = {pullRequestDTOS: [], projectId: '', projectName: '', name: '',
                url: '', token: '', applicationDTO: new ApplicationDTO(), id: '', branchName: ''};

    window.onbeforeunload = e => {
      if (this.eventSource){
        this.eventSource.close();
      }
    };
  }

  ngOnInit(): void {

    this.route.params.subscribe(params => {
      this.applicationId = +params.appId; // (+) converts string 'id' to a number
      this.connectionId = +params.connId;

    });

    this.globalService.getPullRequests(this.connectionId).subscribe(res => {
      this.connectionsWithPullRequestsDTO = new ConnectionWithPullRequestsDTO().deserialize(res);
    }, error => {
      this.toastr.errorToastr('Error', 'Invalid token.');
    });

    this.eventSource = new EventSource('http://localhost:8080/api/notifications/subscribe');

    this.eventSource.addEventListener('connectionWithPullRequestsDTO', event => {
      const notification = JSON.parse(event.data);
      this.connectionsWithPullRequestsDTO = new ConnectionWithPullRequestsDTO().deserialize(notification);
      console.log(this.connectionsWithPullRequestsDTO);

    });

    // todo ugasiti event source kada se izlazi iz metode da ne bi ovde pristizao connectionWithCommitsDTO
    // this.eventSource.onmessage = event => {
    //   const notification = JSON.parse(event.data);
    //   this.connectionsWithPullRequestsDTO = new ConnectionWithPullRequestsDTO().deserialize(notification);
    //   console.log(this.connectionsWithPullRequestsDTO);
    // };
  }

  back(): void {
    this.location.back();
  }

  ngOnDestroy(): void {
    if (this.eventSource){
      this.eventSource.close();
    }
  }

}
