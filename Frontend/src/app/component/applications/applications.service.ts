import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaderResponse, HttpProgressEvent, HttpResponse, HttpSentEvent, HttpUserEvent} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {ApplicationDTO} from '../../model/ApplicationDTO';
import {tap} from 'rxjs/operators';
import {ApplicationWithConnectionsDTO} from '../../model/ApplicationWithConnectionsDTO';
import {query} from '@angular/animations';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsService {



  constructor(private http: HttpClient) { }

  private refreshNeeded = new Subject<void>();

  get refreshNeeded$(): Subject<void> {
    return this.refreshNeeded;
  }

  getApplications(): Observable<ApplicationDTO[]>{
    // return this.http.get<ApplicationDTO[]>('/api/applications/get');
    const queryGraphql = '{\n' +
      '    getApplications{\n' +
      '        id\n' +
      '        name\n' +
      '    }\n' +
      '}';
    return this.http.post<ApplicationDTO[]>('api/graphql', queryGraphql);
  }

  saveApplication(application: any): Observable<ApplicationDTO> {
    return this.http.post<ApplicationDTO>('/api/applications/create', application)
      .pipe(
        tap(() => {
          this.refreshNeeded.next();
        })
      );
  }

  deleteApplication(application: any): Observable<ApplicationDTO> {
    return this.http.put<ApplicationDTO>('/api/applications/delete', application)
      .pipe(
        tap(() => {
          this.refreshNeeded.next();
        })
      );
  }


  updateApplication(application: any): Observable<ApplicationDTO>  {
    return this.http.put<ApplicationDTO>('/api/applications/put', application)
      .pipe(
        tap(() => {
          this.refreshNeeded.next();
        })
      );
  }

}
