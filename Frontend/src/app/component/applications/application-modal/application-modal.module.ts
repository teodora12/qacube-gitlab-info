import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ApplicationModalRoutingModule} from './application-modal-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ApplicationModalRoutingModule,
    FormsModule,
    NgbModule
  ],
  declarations: [],
  providers: [NgbActiveModal]

})

export class ApplicationModalModule { }
