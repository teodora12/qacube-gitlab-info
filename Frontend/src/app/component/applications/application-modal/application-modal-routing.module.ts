import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ApplicationModalComponent} from './application-modal.component';

const routes: Routes = [
  {
    path: '',
    component: ApplicationModalComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ApplicationModalRoutingModule { }
