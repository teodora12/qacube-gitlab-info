import {Component, Input, OnInit} from '@angular/core';
import {ToastrManager} from 'ng6-toastr-notifications';
import {GlobalService} from '../../../service/global.service';
import {ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ConnectionsService} from '../../connections/connections.service';
import {ApplicationsService} from '../applications.service';
import {ApplicationDTO} from '../../../model/ApplicationDTO';

@Component({
  selector: 'app-application-modal',
  templateUrl: './application-modal.component.html',
  styleUrls: ['./application-modal.component.css']
})
export class ApplicationModalComponent implements OnInit {

  @Input() application;
  @Input() showValidationMessage;
  @Input() showApplicationFields;
  @Input() showConnectionFields;
  @Input() btnType;
  @Input() clickedId;
  @Input() connection;
  @Input() applications;
  @Input() ref;
  @Input() closeRes;

  constructor(private toastr: ToastrManager, private globalService: GlobalService,
              public activeModal: NgbActiveModal, private connService: ConnectionsService,
              private  applicationsService: ApplicationsService) { }

  ngOnInit(): void {

    if (this.btnType === 'btnNewApp'){
      this.showApplicationFields = true;
      this.showValidationMessage = false;
      console.log(this.btnType);
    } else if (this.btnType === 'btnNewConn'){
      this.showConnectionFields = true;
      this.showValidationMessage = false;
      console.log(this.btnType);
    } else if (this.btnType === 'btnEditApp'){
      this.showApplicationFields = true;
      this.showValidationMessage = false;
    }


  }

  createOrUpdate(): any {
    if (this.showConnectionFields){
      if (this.connection.name === '' || this.connection.name === undefined || this.connection.name === null
        || this.connection.branchName === '' || this.connection.branchName === undefined || this.connection.branchName === null
        || this.connection.projectName === '' || this.connection.projectName === null || this.connection.projectName === undefined
        || this.connection.url === '' || this.connection.url === undefined || this.connection.url === null
        || this.connection.applicationDTO.name === '' || this.connection.applicationDTO.name === undefined
        || this.connection.applicationDTO.name === null){
        this.toastr.warningToastr('All fields except access token are mandatory!');
        return;
      }
      this.createConnection();
    } else if (this.showApplicationFields) {

      if (this.clickedId === null || this.clickedId === '' || this.clickedId === undefined) {

        if (this.application.name === '' || this.application.name === null || this.application.name === undefined){
          this.toastr.warningToastr('You cannot create application if you do not specify the name!');
          this.showValidationMessage = true;
          return;
        }
        this.saveApplication();
      } else {

        if (this.application.name === '' || this.application.name === undefined || this.application.name === null){
          this.toastr.warningToastr('You cannot update application if you do not specify the name!');
          this.showValidationMessage = true;
          return;
        }
        this.updateApplication();

      }
    }
  }

  saveApplication(): any {

    console.log(this.application);
    console.log('create app');

    this.ref.close();
    this.showApplicationFields = false;

    this.applicationsService.saveApplication(this.application).subscribe(res => {

      this.application = new ApplicationDTO().deserialize(res);

      console.log('SAVE APP ');
      console.log(this.applications);

    }, error => {
      this.toastr.errorToastr('Error', 'Error while creating new application, something went wrong');
    });
  }

  updateApplication(): any{

    this.application.id = this.clickedId;

    this.ref.close();
    this.showApplicationFields = false;

    const updatedApp = new ApplicationDTO().deserialize(this.application);

    this.clickedId = '';
    this.applicationsService.updateApplication(this.application).subscribe(res => {

      this.application = new ApplicationDTO().deserialize(this.application);

      if (this.application instanceof ApplicationDTO) {
        const i = this.applications.findIndex(app => app.id === updatedApp.id);

        this.applications.forEach((element, index) => {
          if (index === i) {
            this.applications[i].name = this.application.name;
          }
        });
      }

    }, error => {
      this.toastr.errorToastr('Error', 'Error while updating the application, something went wrong');
    });

    this.application.id = '';
  }

  createConnection(): any {
    console.log(this.connection );

    this.ref.close();
    this.showConnectionFields = false;

    this.globalService.createConnectionForApplication(this.connection).subscribe( res => {
      console.log(res);

    }, error => {

    });
  }



}
