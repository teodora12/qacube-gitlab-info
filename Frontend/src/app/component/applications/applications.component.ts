import { Component, OnInit } from '@angular/core';
import {ApplicationDTO} from '../../model/ApplicationDTO';
import {ToastrManager} from 'ng6-toastr-notifications';
import {ModalDismissReasons, NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {GlobalService} from '../../service/global.service';
import {ApplicationsService} from './applications.service';
import {ApplicationModalComponent} from './application-modal/application-modal.component';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.css']
})
export class ApplicationsComponent implements OnInit {

  applications: ApplicationDTO[];
  application: { name: string; id: string } = new ApplicationDTO();
  connection: any;
  closeResult = '';
  modalRef: NgbModalRef;
  clickedId: any;
  btnType: string;

  showConnectionFields: boolean;
  showApplicationFields: boolean;
  showValidationMessage: boolean;
  disableBtn: boolean;


  constructor(private applicationsService: ApplicationsService, private toastr: ToastrManager,
              private modalService: NgbModal, private globalService: GlobalService) {
    this.application = { id: '', name: ''};
    this.connection = {id: '', name: '', branchName: '', projectName: '', token: '', url: '', applicationDTO: {id : '', name : ''}};
    this.applications = [];
  }

  ngOnInit(): void {

    this.applicationsService.refreshNeeded$.subscribe(res => {
      console.log(res);
      this.getAllApplications();
    });

    this.getAllApplications();

    this.showConnectionFields = false;
    this.showApplicationFields = false;
    this.showValidationMessage = false;

  }

  getAllApplications(): any{
    this.applicationsService.getApplications().subscribe( (res: any) => {

      for (let applicationDTO of res.data.getApplications){
        applicationDTO = new ApplicationDTO().deserialize(applicationDTO);

        if (applicationDTO instanceof ApplicationDTO) {
          if (!this.applications.some((item) => item.id === applicationDTO.id)) {
            this.applications.push(applicationDTO);
          }
        }
      }

      if (this.applications.length === 0){
        this.disableBtn = true;
      } else {
        this.disableBtn = false;
      }

      this.clearInputFields();


    }, error => {
      this.toastr.warningToastr('There are no applications for display. Try creating new one!');
      this.disableBtn = true;
    });
  }

  open(id, btnType): any{

    this.clickedId = id;

    for (const applicationDTO of this.applications){
      if (applicationDTO.id === this.clickedId) {
        this.application.name = applicationDTO.name;
      }
    }


    this.modalRef = this.modalService.open(ApplicationModalComponent);
    this.modalRef.componentInstance.application = this.application;
    this.modalRef.componentInstance.showValidationMessage = this.showValidationMessage;
    this.modalRef.componentInstance.showApplicationFields = this.showApplicationFields;
    this.modalRef.componentInstance.showConnectionFields = this.showConnectionFields;
    this.modalRef.componentInstance.btnType = btnType;
    this.modalRef.componentInstance.clickedId = this.clickedId;
    this.modalRef.componentInstance.connection = this.connection;
    this.modalRef.componentInstance.applications = this.applications;
    this.modalRef.componentInstance.ref = this.modalRef;
    this.modalRef.componentInstance.closeRes = this.closeResult;

    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });

  }


  private getDismissReason(reason: any): string {
    this.clearInputFields();
    console.log('usao');
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else if (reason === 'Cross click') {
      return 'by clicking on a x';
    } else if (reason === 'Cancel'){
      return 'by clicking on cancel';
    }else {
      return `with: ${reason}`;
    }
  }

  deleteApplication(id: any): any{

    this.application.id = id;

    this.applicationsService.deleteApplication(this.application).subscribe(res => {

      this.application = new ApplicationDTO().deserialize(this.application);

      if (this.application instanceof ApplicationDTO) {
        const i = this.applications.findIndex(app => app.id === this.application.id);

        this.applications.forEach((element, index) => {
          if (index === i) {
            this.applications.splice(i, 1);
          }
        });
      }

      this.application = {id: '', name: ''};
      console.log('DELETE APP! ');
      console.log(res);
    }, error => {
      this.toastr.errorToastr('Error', 'Error while deleting the application, something went wrong');

    });
  }


  clearInputFields(): void {
    this.showConnectionFields = false;
    this.showApplicationFields = false;
    this.connection = {id: '', name: '', branchName: '', projectName: '', token: '', url: '', applicationDTO: {id : '', name : ''}};
    this.application = {id: '', name: ''};
    this.clickedId = '';
  }

}
