import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ApplicationsComponent} from './component/applications/applications.component';
import {ConnectionModalComponent} from './component/connections/connection-modal/connection-modal.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/applications',
    pathMatch: 'full'
  },
  {
    path: 'applications',
    component: ApplicationsComponent,
    loadChildren: () => import('./component/applications/application-modal/application-modal.module').then(m => m.ApplicationModalModule)
  },
  {
    path: 'applications/:id/connections',
    loadChildren: () => import('./component/connections/connections.module').then(m => m.ConnectionsModule)
  },
  {
    path: 'applications/:appId/connections/:connId/pullRequests',
    loadChildren: () => import('./component/pull-requests/pull-requests.module').then(m => m.PullRequestsModule)
  },
  {
    path: 'applications/:appId/connections/:connId/commits',
    loadChildren: () => import('./component/commits/commits.module').then(m => m.CommitsModule)
  }
  // {
  //   path: 'applications/:id/connections', component: ConnectionModalComponent
  //   // loadChildren: () => import('./component/connections/connection-modal/connection-modal.module').then(m => m.ConnectionModalModule)
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
