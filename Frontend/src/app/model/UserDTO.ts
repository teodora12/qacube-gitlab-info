import {Deserializable} from './deserializable.model';

export class UserDTO implements Deserializable{

  constructor() {
  }

  id: number;
  name: string;
  username: string;

  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }


}
