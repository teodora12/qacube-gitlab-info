import {Deserializable} from './deserializable.model';

export class CommitDTO implements Deserializable{

  constructor() {
  }


  id: string;
  shortId: string;
  createdAt: string;
  message: string;
  authorName: string;
  authorEmail: string;
  authoredDate: string;
  committerName: string;
  committerEmail: string;
  committedDate: string;
  webUrl: string;


  deserialize(input: any): this {
    this.id = input.id;
    this.shortId = input.short_id;
    this.createdAt = input.created_at;
    this.message = input.message;
    this.authorName = input.author_name;
    this.authorEmail = input.author_email;
    this.authoredDate = input.authored_date;
    this.committerName = input.committer_name;
    this.committedDate = input.committed_date;
    this.webUrl = input.web_url;

    return this;
  }

}
