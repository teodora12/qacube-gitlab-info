import {ConnectionDTO} from './ConnectionDTO';
import {Deserializable} from './deserializable.model';

export class ApplicationWithConnectionsDTO implements Deserializable{

  constructor() {
    this.connectionDTOS = [];
  }

  id: string;
  name: string;
  connectionDTOS: Array<ConnectionDTO>;

  deserialize(input: any): this {

    this.id = input.applicationDTO.id;
    this.name = input.applicationDTO.name;

    for (let connectionDTO of input.connectionDTOS){
      if (connectionDTO.token === null){
        connectionDTO.token = '';
      }
      connectionDTO = new ConnectionDTO().deserialize(connectionDTO);
      this.connectionDTOS.push(connectionDTO);
    }
    return this;
  }


}
