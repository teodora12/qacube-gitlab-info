import {Deserializable} from './deserializable.model';

export class ApplicationDTO implements Deserializable{

  constructor() {
  }

  id: string;
  name: string;

  deserialize(input: any): this {

    Object.assign(this, input);
    return this;
  }
  // connections: any;

}
