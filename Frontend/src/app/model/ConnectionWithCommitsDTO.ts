import {Deserializable} from './deserializable.model';
import {ApplicationDTO} from './ApplicationDTO';
import {CommitDTO} from './CommitDTO';

export class ConnectionWithCommitsDTO  implements Deserializable {

  constructor() {
    this.commitDTOS = [];
  }

  id: string;
  name: string;
  url: string;
  token: string;
  projectName: string;
  branchName: string;
  applicationDTO: ApplicationDTO;
  commitDTOS: CommitDTO[];
  projectId: string;

  deserialize(input: any): this {

    this.id = input.id;
    this.name = input.name;
    this.url = input.url;
    this.token = input.token;
    this.projectName = input.projectName;
    this.branchName = input.branchName;
    this.applicationDTO = new ApplicationDTO().deserialize(input.applicationDTO);
    this.projectId = input.project_id;

    for (let commitDTO of input.commitDTOS){
      commitDTO = new CommitDTO().deserialize(commitDTO);
      this.commitDTOS.push(commitDTO);
    }
    return this;
  }



}
