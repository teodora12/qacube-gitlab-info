import {ApplicationDTO} from './ApplicationDTO';
import {Deserializable} from './deserializable.model';

export class ConnectionDTO implements Deserializable{

  constructor() {
  }

  id: string;
  name: string;
  url: string;
  token: string;
  projectName: string;
  branchName: string;
  applicationDTO: ApplicationDTO;

  deserialize(input: any): this {
    Object.assign(this, input);
    this.applicationDTO = new ApplicationDTO().deserialize(input.applicationDTO);
    return this;
  }

}
