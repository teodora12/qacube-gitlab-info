import {Deserializable} from './deserializable.model';
import {UserDTO} from './UserDTO';

export class PullRequestDTO implements Deserializable{

  constructor() {
  }


  id: number;
  title: string;
  createdAt: string;
  updatedAt: string;
  mergedAt: string;
  targetBranch: string;
  sourceBranch: string;
  author: UserDTO;
  assignee: UserDTO;

  deserialize(input: any): this {

    this.id = input.id;
    this.title = input.title;
    this.createdAt = input.created_at;
    this.updatedAt = input.updated_at;
    this.mergedAt = input.merged_at;
    this.targetBranch = input.target_branch;
    this.sourceBranch = input.source_branch;
    this.author = new UserDTO().deserialize(input.author);
    this.assignee = new UserDTO().deserialize(input.assignee);

    return this;
  }

}
