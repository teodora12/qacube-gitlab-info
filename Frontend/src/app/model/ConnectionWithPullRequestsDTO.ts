import {Deserializable} from './deserializable.model';
import {PullRequestDTO} from './PullRequestDTO';
import {ApplicationDTO} from './ApplicationDTO';

export class ConnectionWithPullRequestsDTO  implements Deserializable {

  constructor() {
    this.pullRequestDTOS = [];
  }

  id: string;
  name: string;
  url: string;
  token: string;
  applicationDTO: ApplicationDTO;
  projectName: string;
  branchName: string;
  pullRequestDTOS: PullRequestDTO[];
  projectId: string;


  deserialize(input: any): this {

    this.id = input.id;
    this.name = input.name;
    this.url = input.url;
    this.token = input.token;
    this.applicationDTO = new ApplicationDTO().deserialize(input.applicationDTO);
    this.projectName = input.projectName;
    this.branchName = input.branchName;
    this.projectId = input.project_id;

    // console.log(input.pullRequestDTOS);

    for (let pullRequestDTO of input.pullRequestDTOS){
      pullRequestDTO = new PullRequestDTO().deserialize(pullRequestDTO);
      this.pullRequestDTOS.push(pullRequestDTO);
    }
    return this;
  }

}
