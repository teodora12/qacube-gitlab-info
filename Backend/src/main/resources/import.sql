insert into application (id, name) values (1, 'Batman application');
insert into application (id, name) values (2, 'QaCube application');
insert into application (id, name) values (3, 'Inkscape application');

insert into connection (id, name, branch_name, project_name, project_id, token, url, application_id) values (1, 'Batman connection 1', 'batman_1.9.1', 'batman', 4319147, '', 'https://gitlab.com/cerfacs/batman', 1);
insert into connection (id, name, branch_name, project_name, project_id, token, url, application_id) values (2, 'Batman connection 2', 'develop', 'batman', 4319147,'', 'https://gitlab.com/cerfacs/batman', 1);
insert into connection (id, name, branch_name, project_name, project_id, token, url, application_id) values (3, 'Batman connection 3', 'developp_hadri', 'batman', 4319147, '', 'https://gitlab.com/cerfacs/batman', 1);

insert into connection (id, name, branch_name, project_name, project_id, token, url, application_id) values (4, 'Qacube connection Teodora', 'master', 'qacube-gitlab-info', 26450992, '', 'https://gitlab.com/teodora12/qacube-gitlab-info', 2);
insert into connection (id, name, branch_name, project_name, project_id, token, url, application_id) values (5, 'Qacube connection Masa', 'master', 'QACube Jenkins Jobs', 26420433, '', 'https://gitlab.com/marijamilanovic/qacube-jenkins-jobs', 2);
insert into connection (id, name, branch_name, project_name, project_id, token, url, application_id) values (6, 'Qacube connection Milica', 'master', 'QaCube', 26742054, '', 'https://gitlab.com/arsovicmilica/qacube', 2);

insert into connection (id, name, branch_name, project_name, project_id, token, url, application_id) values (7, 'Inkscape connection', 'master', 'inkscape', 26873509, 'DUsWxbLpsxfz6yCUgpjf', 'https://gitlab.com/teodora12/inkscape', 3);
