package qacube.gitlabinfo.backend.controller;

import org.gitlab4j.api.CommitsApi;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.ProjectApi;
import org.gitlab4j.api.models.Session;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import qacube.gitlabinfo.backend.DTO.ApplicationWithConnectionsDTO;
import qacube.gitlabinfo.backend.DTO.ConnectionDTO;
import qacube.gitlabinfo.backend.DTO.ConnectionWithCommitsDTO;
import qacube.gitlabinfo.backend.DTO.ConnectionsWithPullRequestsDTO;
import qacube.gitlabinfo.backend.model.utils.CommitUtils;
import qacube.gitlabinfo.backend.model.utils.PullRequestUtils;
import qacube.gitlabinfo.backend.service.ConnectionService;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping(value = "/api/connections")
public class ConnectionController {

    private ConnectionService connectionService;

    public ConnectionController(ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @GetMapping(value = "/get")
    public ResponseEntity getConnections() {

        List<ConnectionDTO> connectionDTOS = this.connectionService.getAll();

        if(connectionDTOS.isEmpty() || connectionDTOS == null){
            return ResponseEntity.notFound().build();
        }

        return new ResponseEntity(connectionDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/get/applicationId/{id}")
    public ResponseEntity getConnectionsForApplication(@PathVariable @NotBlank String id) {

        List<ConnectionDTO> connectionDTOS = this.connectionService.getConnectionsByApplicationId(Long.parseLong(id));

        if(connectionDTOS.isEmpty() || connectionDTOS == null){
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity(connectionDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/get/application/{id}")
    public ResponseEntity getApplicationWithConnections(@PathVariable @NotBlank String id) {

        ApplicationWithConnectionsDTO applicationWithConnectionsDTO = this.connectionService.getApplicationWithConnections(Long.parseLong(id));

        if(applicationWithConnectionsDTO == null){
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity(applicationWithConnectionsDTO, HttpStatus.OK);
    }

    @RequestMapping(value = "/put", method = {RequestMethod.PUT, RequestMethod.GET})
    public ResponseEntity updateConnection(@RequestBody @NotNull ConnectionDTO connectionDTO) {

        if(connectionDTO.getId() == null || connectionDTO.getId().equals("")){
            return ResponseEntity.notFound().build();
        }

        ConnectionDTO returnConnectionDTO = this.connectionService.updateConnection(connectionDTO);

        if(returnConnectionDTO == null){
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity(returnConnectionDTO, HttpStatus.OK);

    }

    @RequestMapping(value = "/delete", method = {RequestMethod.PUT, RequestMethod.GET})
    public ResponseEntity deleteConnection(@RequestBody @NotNull ConnectionDTO connectionDTO) {

        if(connectionDTO.getId() == null){
            return ResponseEntity.notFound().build();
        }

        boolean success = connectionService.deleteConnection(connectionDTO);

        if(success){
            return new ResponseEntity(true, HttpStatus.OK);
        }

        return ResponseEntity.notFound().build();

    }

    @PostMapping(value = "/create")
    public ResponseEntity createConnection(@RequestBody @NotNull ConnectionDTO connectionDTO) {


        ConnectionDTO returnConnectionDTO = connectionService.createConnection(connectionDTO);

        if(returnConnectionDTO == null || returnConnectionDTO.getId() == null){
            return ResponseEntity.notFound().build();
        }

        return new ResponseEntity(returnConnectionDTO, HttpStatus.OK);

    }

    @GetMapping(value = "/{id}/commits")
    public ResponseEntity getCommits(@PathVariable @NotBlank String id) {

        ConnectionWithCommitsDTO connectionWithCommitsDTO = connectionService.getConnectionCommits(Long.parseLong(id));

        if(connectionWithCommitsDTO == null){
            return ResponseEntity.notFound().build();
        }
        CommitUtils.connectionWithCommitsDTO = connectionWithCommitsDTO;

        return new ResponseEntity(connectionWithCommitsDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/pullRequests")
    public ResponseEntity getPullRequests(@PathVariable @NotBlank String id) {

        ConnectionsWithPullRequestsDTO connectionsWithPullRequestsDTO = connectionService.getConnectionPullRequests(Long.parseLong(id));

        if(connectionsWithPullRequestsDTO == null){
            return ResponseEntity.notFound().build();
        }
        PullRequestUtils.connectionsWithPullRequestsDTO = connectionsWithPullRequestsDTO;

        return new ResponseEntity(connectionsWithPullRequestsDTO, HttpStatus.OK);
    }

}
