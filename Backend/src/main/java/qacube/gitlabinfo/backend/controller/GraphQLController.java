package qacube.gitlabinfo.backend.controller;

import graphql.ExecutionResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import qacube.gitlabinfo.backend.service.GraphQLService;

@RestController
@RequestMapping("/api/graphql")
@CrossOrigin("http://localhost:4200")
public class GraphQLController {


     @Autowired
     private GraphQLService graphQLService;

     @PostMapping
     public ResponseEntity getApplications(@RequestBody String query){
          ExecutionResult executionResult = graphQLService.getGraphQL().execute(query);

          return new ResponseEntity(executionResult, HttpStatus.OK);
     }

}
