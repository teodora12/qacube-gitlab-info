package qacube.gitlabinfo.backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import qacube.gitlabinfo.backend.DTO.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@RestController
@RequestMapping(value = "/api/notifications")
@CrossOrigin("http://localhost:4200")
public class WebPushNotificationsController {

    private final CopyOnWriteArrayList<SseEmitter> emitters = new CopyOnWriteArrayList<>();
    private static Logger logger = LoggerFactory.getLogger(WebPushNotificationsController.class);


    @RequestMapping(value = "/subscribe", consumes = MediaType.ALL_VALUE)
    public SseEmitter subscribe() {

        SseEmitter sseEmitter = new SseEmitter();

        logger.info("SseEmitter initialization");
        this.emitters.add(sseEmitter);

        sseEmitter.onCompletion(() -> {
            this.emitters.remove(sseEmitter);
            logger.info("SseEmitter method onCompletion()");
        });
        sseEmitter.onTimeout(() -> {
            sseEmitter.complete();
            this.emitters.remove(sseEmitter);
            logger.info("SseEmitter method onTimeout()");
        });
        return sseEmitter;
    }

    @EventListener
    public void onCommitNotification(Object o) {
        List<SseEmitter> deadEmitters = new ArrayList<>();

        this.emitters.forEach(emitter -> {
            try {

                if(o instanceof ConnectionWithCommitsDTO){
                    // FOR TESTING COMMIT LIST REFRESH
//                    CommitDTO commitDTO = new CommitDTO();
//                    commitDTO.setId("f54re1658f4d654fffffffffff");
//                    commitDTO.setCommitter_email("TEST@gmail.com");
//                    commitDTO.setAuthor_email("TEST@gmail.com");
//                    commitDTO.setAuthor_name("T. T.");
//                    commitDTO.setCreated_at("12.12.2021. 14:45:00");
//                    commitDTO.setWeb_url("https://gitlab.com/test/testRepository");
//                    commitDTO.setCommitter_email("t. t.");
//                    commitDTO.setMessage("TEST MESSAGE");
//                    commitDTO.setShort_id("112");
//                    commitDTO.setAuthored_date("12.21.2021. 16:42:25");
//
//                    ((ConnectionWithCommitsDTO) o).getCommitDTOS().add(commitDTO);

                    emitter.send(SseEmitter.event().name("connectionWithCommitsDTO").data(o));
                    logger.info("Notification commit sent");
                } else if (o instanceof ConnectionsWithPullRequestsDTO){

                    // FOR TESTING PULL REQUEST LIST REFRESH
//                    PullRequestDTO pullRequestDTO = new PullRequestDTO();
//                    pullRequestDTO.setCreated_at("test");
//                    pullRequestDTO.setMerged_at("test");
//                    pullRequestDTO.setSource_branch("test");
//                    pullRequestDTO.setTarget_branch("test");
//                    pullRequestDTO.setTitle("TITLE TEST");
//                    pullRequestDTO.setUpdated_at("test");
//                    pullRequestDTO.setAssignee(new UserDTO());
//                    pullRequestDTO.setAuthor(new UserDTO());
//
//                    ((ConnectionsWithPullRequestsDTO) o).getPullRequestDTOS().add(pullRequestDTO);

                    emitter.send(SseEmitter.event().name("connectionWithPullRequestsDTO").data(o));
                    logger.info("Notification pull request sent");
                }

            } catch (Exception e) {
                deadEmitters.add(emitter);
                logger.error("onNotification catch clause: ERROR " + e.getMessage());
            }
        });
        this.emitters.remove(deadEmitters);
    }


}
