package qacube.gitlabinfo.backend.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import qacube.gitlabinfo.backend.DTO.ApplicationDTO;

import qacube.gitlabinfo.backend.service.ApplicationService;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping(value = "/api/applications")
@CrossOrigin("http://localhost:4200")
public class ApplicationController {

    private ApplicationService applicationService;

    public ApplicationController(ApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    @GetMapping(value = "/get")
    public ResponseEntity getApplications() {

        List<ApplicationDTO> applicationDTOS = this.applicationService.getAll();

        if(applicationDTOS.isEmpty() || applicationDTOS == null){
            return ResponseEntity.notFound().build();
        }

        return new ResponseEntity(applicationDTOS, HttpStatus.OK);

    }

    @PostMapping(value = "/create")
    public ResponseEntity createApplication(@RequestBody @NotNull ApplicationDTO applicationDTO){

        if(applicationDTO.getId() != null && !applicationDTO.getId().equals("")){
            return ResponseEntity.notFound().build();
        }

        ApplicationDTO returnApplicationDTO = this.applicationService.createApplication(applicationDTO);

        if(returnApplicationDTO.getName().equals("null")){
            return new ResponseEntity(returnApplicationDTO, HttpStatus.BAD_REQUEST);
        } else if(returnApplicationDTO.getName().equals("empty")){
            return new ResponseEntity(returnApplicationDTO, HttpStatus.NOT_MODIFIED);
        } else if (returnApplicationDTO == null){
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity(returnApplicationDTO, HttpStatus.OK);

    }


    @RequestMapping(value = "/delete", method = {RequestMethod.PUT, RequestMethod.GET})
    public ResponseEntity deleteApplication(@RequestBody @NotNull ApplicationDTO applicationDTO) {

        if(applicationDTO.getId() == null){
            return ResponseEntity.notFound().build();
        }

        boolean success = this.applicationService.deleteApplication(Long.parseLong(applicationDTO.getId()));

        if (success){
            return new ResponseEntity(true, HttpStatus.OK);
        }

        return ResponseEntity.badRequest().build();

    }

    @RequestMapping(value = "/put", method = {RequestMethod.PUT, RequestMethod.GET})
    public ResponseEntity updateApplication(@RequestBody @NotNull ApplicationDTO applicationDTO) {

        if (applicationDTO.getId() == null || applicationDTO.getName() == null) {
            return ResponseEntity.notFound().build();
        }
        ApplicationDTO returnApplicationDTO = this.applicationService.updateApplication(applicationDTO);

        if(returnApplicationDTO == null){
            return ResponseEntity.badRequest().build();
        }
        return new ResponseEntity(returnApplicationDTO, HttpStatus.OK);

    }


}
