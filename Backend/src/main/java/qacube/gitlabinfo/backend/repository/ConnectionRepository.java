package qacube.gitlabinfo.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import qacube.gitlabinfo.backend.model.Application;
import qacube.gitlabinfo.backend.model.Connection;

import java.util.List;

@Repository
public interface ConnectionRepository extends JpaRepository<Connection, Long> {

    List<Connection> findConnectionsByApplication(Application application);
    Connection findConnectionById(Long id);
    Connection findConnectionByName(String name);

}
