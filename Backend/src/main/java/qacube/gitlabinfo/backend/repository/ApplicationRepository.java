package qacube.gitlabinfo.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import qacube.gitlabinfo.backend.model.Application;

import java.util.Set;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {

    Application findApplicationById(Long id);
    Application findApplicationByName(String name);

}
