package qacube.gitlabinfo.backend.service.dataFetcher;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qacube.gitlabinfo.backend.DTO.ApplicationDTO;
import qacube.gitlabinfo.backend.service.ApplicationService;

import java.util.List;

@Component
public class ApplicationsDataFetcher implements DataFetcher<List<ApplicationDTO>> {

    @Autowired
    private ApplicationService applicationService;

    @Override
    public List<ApplicationDTO> get(DataFetchingEnvironment dataFetchingEnvironment) throws Exception {
        return applicationService.getAll();
    }
}
