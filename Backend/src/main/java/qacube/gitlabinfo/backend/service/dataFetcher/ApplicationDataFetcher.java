package qacube.gitlabinfo.backend.service.dataFetcher;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qacube.gitlabinfo.backend.DTO.ApplicationWithConnectionsDTO;
import qacube.gitlabinfo.backend.service.ConnectionService;

@Component
public class ApplicationDataFetcher implements DataFetcher<ApplicationWithConnectionsDTO> {

    @Autowired
    private ConnectionService connectionService;

    @Override
    public ApplicationWithConnectionsDTO get(DataFetchingEnvironment dataFetchingEnvironment) throws Exception {

        String isn = dataFetchingEnvironment.getArgument("id");

        return connectionService.getApplicationWithConnections(Long.parseLong(isn));
    }

//    public ApplicationWithConnectionsDTO getApplicationWithConnections(Long id){
//        return connectionService.getApplicationWithConnections(id);
//    }
}
