package qacube.gitlabinfo.backend.service.serviceImpl;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import qacube.gitlabinfo.backend.DTO.*;
import qacube.gitlabinfo.backend.model.Application;
import qacube.gitlabinfo.backend.model.Connection;
import qacube.gitlabinfo.backend.model.utils.*;
import qacube.gitlabinfo.backend.repository.ConnectionRepository;
import qacube.gitlabinfo.backend.service.ApplicationService;
import qacube.gitlabinfo.backend.service.ConnectionService;

import javax.persistence.NonUniqueResultException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class ConnectionServiceImpl implements ConnectionService {

//    public static String PROJECTS_URL_GITLAB = "https://gitlab.com/api/v4/projects?search=";
//    public static String PROJECT_URL_PREFIX = "https://gitlab.com/api/v4/projects/";
//    public static String PROJECT_COMMITS_SUFFIX = "/repository/commits?ref_name=";
//    public static String PROJECT_PULL_REQUESTS_SUFFIX = "/merge_requests?target_branch=";
    private static Logger logger = LoggerFactory.getLogger(ConnectionServiceImpl.class);


    private ConnectionRepository connectionRepository;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private RestTemplate myRestTemplate;

    HttpEntity entity;

    public ConnectionServiceImpl(ConnectionRepository connectionRepository) {
        this.connectionRepository = connectionRepository;
    }


    @Override
    public List<ConnectionDTO> getAll() {
        List<Connection> connections= this.connectionRepository.findAll();
        List<ConnectionDTO> connectionDTOS = new ArrayList<>();
        for(Connection c: connections){
            ConnectionDTO connectionDTO = ConnectionUtils.connectionToDTO(c);
            connectionDTOS.add(connectionDTO);
        }

        return connectionDTOS;
    }

    @Override
    public List<ConnectionDTO> getConnectionsByApplicationId(Long id) {

        Application application = this.applicationService.getApplicationById(id);

        List<Connection> connections= this.connectionRepository.findConnectionsByApplication(application);
        List<ConnectionDTO> connectionDTOS = new ArrayList<>();
        for(Connection c: connections){
            ConnectionDTO connectionDTO = ConnectionUtils.connectionToDTO(c);
            connectionDTOS.add(connectionDTO);
        }

        return connectionDTOS;
    }

    @Override
    public boolean deleteConnection(ConnectionDTO connectionDTO) {

        Connection connection = connectionRepository.findConnectionById(Long.parseLong(connectionDTO.getId()));
        if(connection == null || connection.getId() == null){
            return false;
        }
        try {
            connectionRepository.delete(connection);

        } catch (Exception e){
            e.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public ApplicationWithConnectionsDTO getApplicationWithConnections(Long id) {

        Application application = this.applicationService.getApplicationById(id);
        List<Connection> connections= this.connectionRepository.findConnectionsByApplication(application);
        ApplicationWithConnectionsDTO applicationWithConnectionsDTO = new ApplicationWithConnectionsDTO();

        applicationWithConnectionsDTO.getApplicationDTO().setId(String.valueOf(application.getId()));
        applicationWithConnectionsDTO.getApplicationDTO().setName(application.getName());

        for(Connection c: connections){
            ConnectionDTO connectionDTO = ConnectionUtils.connectionToDTO(c);
            applicationWithConnectionsDTO.getConnectionDTOS().add(connectionDTO);
        }

        return applicationWithConnectionsDTO;
    }

    @Override
    public ConnectionDTO updateConnection(ConnectionDTO connectionDTO) {

        Connection connection = this.connectionRepository.findConnectionById(Long.parseLong(connectionDTO.getId()));

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");


        if(connection == null){
            logger.error("Cannot update connection with ID: " + connectionDTO.getId() + " since it does not exist in database.");
            return null;
        }
        else if( !(connection.getApplication().getId().equals(Long.parseLong(connectionDTO.getApplicationDTO().getId()))) ){
            logger.error("Application ID of given connection does not fit with application ID from database.");
            return null;
        }
//        Application application = this.applicationService.getApplicationById(Long.parseLong(connectionDTO.getApplicationDTO().getId()));

        Application application = applicationService.getApplicationByName(connectionDTO.getApplicationDTO().getName());
        if(application == null){
            logger.error("Application name of given connection does not exist in database.");
            return null;
        }

        if(!connectionDTO.getName().equals("")) {
//            if( connectionRepository.findConnectionByName(connectionDTO.getName()) != null){
//                return null;
//            }
            connection.setName(connectionDTO.getName());
        }
        if(connectionDTO.getToken().equals("") || connectionDTO.getToken() == null){
            connection.setToken("");
        } else {
            connection.setToken(connectionDTO.getToken());
            headers.setBearerAuth(connectionDTO.getToken());
        }
        connection.setApplication(application);

        if(!connectionDTO.getProjectName().equals(connection.getProjectName()) ||
        !connectionDTO.getUrl().equals(connection.getUrl()) ||
        !connectionDTO.getBranchName().equals(connection.getBranchName())){

            if(!connectionDTO.getUrl().equals("")) {
                connection.setUrl(connectionDTO.getUrl());
            }
            if(!connectionDTO.getBranchName().equals("")) {
                connection.setBranchName(connectionDTO.getBranchName());
            }
            if(!connectionDTO.getProjectName().equals("")) {
                connection.setProjectName(connectionDTO.getProjectName());
            }

            entity = new HttpEntity(headers);

            String url = UrlUtils.PROJECTS_URL_GITLAB + connection.getProjectName();

            try{
                ResponseEntity<ProjectDTO[]> res = myRestTemplate.exchange(url, HttpMethod.GET, entity, ProjectDTO[].class);

                for(ProjectDTO p: Objects.requireNonNull(res.getBody())){
                    if(p.getName().equals(connection.getProjectName())){

                        String username = connectionDTO.getUrl().split("/")[3];

                        if(username.equals(p.getNamespace().getPath()) && connectionDTO.getUrl().equals(p.getWeb_url())){
                            connection.setProjectId(p.getId());
                            break;
                        }
                    }
                }

                try {
                    connectionRepository.save(connection);
                    logger.info("Connection: " + connection.getName() +  ", ID: " + connection.getId() + " is updated and stored into database.");
                } catch (NonUniqueResultException e){
                    logger.error("Cannot update connection with ID: " + connectionDTO.getId() + ".");
                    return null;
                }

                connectionDTO.setApplicationDTO(ApplicationUtils.applicationToDTO(application));

                return connectionDTO;

            } catch (HttpClientErrorException e){
                logger.error("Rest template gitlab API call ERROR: " + e.getMessage() + ".");
                return null;
            }


        } else {
            if(!connectionDTO.getUrl().equals("")) {
                connection.setUrl(connectionDTO.getUrl());
            }
            if(!connectionDTO.getBranchName().equals("")) {
                connection.setBranchName(connectionDTO.getBranchName());
            }
            if(!connectionDTO.getProjectName().equals("")) {
                connection.setProjectName(connectionDTO.getProjectName());
            }

            connectionRepository.save(connection);
            logger.info("Connection: " + connection.getName() +  ", ID: " + connection.getId() + " is updated and stored into database.");

            connectionDTO.setApplicationDTO(ApplicationUtils.applicationToDTO(application));

            return connectionDTO;

        }

    }

    @Override
    public ConnectionDTO createConnection(ConnectionDTO connectionDTO) {

        Application application = new Application();
        Connection connection = ConnectionUtils.connectionDTOToConnection(connectionDTO);

        if(!connectionDTO.getName().equals("")) {
            if( connectionRepository.findConnectionByName(connectionDTO.getName()) != null){
                logger.error("Connection with name: " + connection.getName() +  " already exists.");
                return null;
            }
        }

        if(connectionDTO.getApplicationDTO().getId() != null && !connectionDTO.getApplicationDTO().getId().equals("")){
            if(!connectionDTO.getApplicationDTO().getName().equals("") && connectionDTO.getApplicationDTO().getName() != null) {
                application = applicationService.getApplicationById(Long.parseLong(connectionDTO.getApplicationDTO().getId()));

                if(!application.getName().equals(connectionDTO.getApplicationDTO().getName()) || application.getName() == null){
                    logger.error("Application name of given connection does not fit with application name from database.");
                    return null;
                }

                connection.getApplication().setName(application.getName());
                connection.getApplication().setId(application.getId());

                applicationService.saveApplication(application);
                logger.info("Application: " + application.getName() + ", ID: " + application.getId() + " is updated and stored into database.");

            }
        }

        if(connectionDTO.getApplicationDTO().getId().equals("") || connectionDTO.getApplicationDTO().getId() == null){
            if(!connectionDTO.getApplicationDTO().getName().equals("") && connectionDTO.getApplicationDTO().getName() != null){
                application = applicationService.getApplicationByName(connectionDTO.getApplicationDTO().getName());

                if(!application.getName().equals(connectionDTO.getApplicationDTO().getName()) || application.getName() == null){
                    logger.error("Application name of given connection does not fit with application name from database.");
                    return null;
                }
                connection.getApplication().setId(application.getId());
                connectionDTO.getApplicationDTO().setId(String.valueOf(connection.getId()));

                applicationService.saveApplication(application);
                logger.info("Application: " + application.getName() + ", ID: " + application.getId() + " is updated and stored into database.");
            }

        }

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");


        if(connectionDTO.getToken() != null && !connectionDTO.getToken().equals("")){
            headers.setBearerAuth(connectionDTO.getToken());
        }

        entity = new HttpEntity(headers);

        String url = UrlUtils.PROJECTS_URL_GITLAB + connection.getProjectName();

        try {
            ResponseEntity<ProjectDTO[]> res = myRestTemplate.exchange(url, HttpMethod.GET, entity, ProjectDTO[].class);
            for(ProjectDTO p: Objects.requireNonNull(res.getBody())){
                if(p.getName().equals(connection.getProjectName())){

                    String username = connectionDTO.getUrl().split("/")[3];

                    if(username.equals(p.getNamespace().getPath()) && connectionDTO.getUrl().equals(p.getWeb_url())){
                        connection.setProjectId(p.getId());
                        break;
                    }
                }
            }

            try {
                connectionRepository.save(connection);
                logger.info("Connection: " + connection.getName() + ", ID: " + connection.getId() + " is created and stored into database.");
            } catch (NonUniqueResultException e){
                logger.error("Could not save connection into database. ERROR: " + e.getMessage() + ".");
                return null;
            }
            connectionDTO.setId(String.valueOf(connection.getId()));
            connectionDTO.setProjectId(String.valueOf(connection.getProjectId()));

            return connectionDTO;
        } catch (HttpClientErrorException e){
            logger.error("Rest template gitlab API call ERROR: " + e.getMessage() + ".");
            return null;
        }


    }

    @Override
    public Connection getConnectionById(Long id) {
        return connectionRepository.findConnectionById(id);
    }

    @Override
    public ConnectionWithCommitsDTO getConnectionCommits(Long id) {

        Connection connection = connectionRepository.findConnectionById(id);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        if(connection.getToken() != null && !connection.getToken().equals("")){
            headers.setBearerAuth(connection.getToken());
        }

        HttpEntity entity = new HttpEntity(headers);

        String url = UrlUtils.PROJECT_URL_PREFIX + connection.getProjectId() + UrlUtils.PROJECT_COMMITS_SUFFIX + connection.getBranchName();

        try {
            ResponseEntity<CommitDTO[]> res = myRestTemplate.exchange(url, HttpMethod.GET, entity, CommitDTO[].class, 1);
            return CommitUtils.commitListMapping(Objects.requireNonNull(res.getBody()), ConnectionUtils.connectionToDTO(connection));

        }catch (HttpClientErrorException e){
            logger.error("Rest template gitlab API call ERROR: " + e.getMessage() + ".");
            return null;
        }

    }

    @Override
    public ConnectionsWithPullRequestsDTO getConnectionPullRequests(Long id) {

        Connection connection = connectionRepository.findConnectionById(id);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        if(connection.getToken() != null && !connection.getToken().equals("")){
            headers.setBearerAuth(connection.getToken());
        }

        HttpEntity entity = new HttpEntity(headers);

        String url = UrlUtils.PROJECT_URL_PREFIX + connection.getProjectId() + UrlUtils.PROJECT_PULL_REQUESTS_SUFFIX + connection.getBranchName();

        try {

            ResponseEntity<PullRequestDTO[]> res = myRestTemplate.exchange(url, HttpMethod.GET, entity, PullRequestDTO[].class, 1);
            return PullRequestUtils.pullRequestListMapping(Objects.requireNonNull(res.getBody()), ConnectionUtils.connectionToDTO(connection));

        } catch (HttpClientErrorException e){
            logger.error("Rest template gitlab API call ERROR: " + e.getMessage() + ".");
            return null;
        }

    }
}
