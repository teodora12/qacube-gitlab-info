package qacube.gitlabinfo.backend.service.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import qacube.gitlabinfo.backend.DTO.CommitDTO;
import qacube.gitlabinfo.backend.DTO.ConnectionWithCommitsDTO;
import qacube.gitlabinfo.backend.DTO.ConnectionsWithPullRequestsDTO;
import qacube.gitlabinfo.backend.DTO.PullRequestDTO;
import qacube.gitlabinfo.backend.model.Connection;
import qacube.gitlabinfo.backend.model.utils.CommitUtils;
import qacube.gitlabinfo.backend.model.utils.ConnectionUtils;
import qacube.gitlabinfo.backend.model.utils.PullRequestUtils;
import qacube.gitlabinfo.backend.model.utils.UrlUtils;
import qacube.gitlabinfo.backend.service.ConnectionService;

import java.util.Objects;

@Service
public class WebPushNotificationsService {

    public final ApplicationEventPublisher applicationEventPublisher;

    private RestTemplate myRestTemplate;

    private ConnectionService connectionService;

    private  ConnectionWithCommitsDTO connectionWithCommitsDTO;

    private  ConnectionsWithPullRequestsDTO connectionsWithPullRequestsDTO;

    private static Logger logger = LoggerFactory.getLogger(WebPushNotificationsService.class);


    public WebPushNotificationsService(ApplicationEventPublisher applicationEventPublisher,
                                       RestTemplate myRestTemplate, ConnectionService connectionService) {
        this.applicationEventPublisher = applicationEventPublisher;
        this.myRestTemplate = myRestTemplate;
        this.connectionService = connectionService;
    }

    @Scheduled(fixedDelay = 20000)
    public void publishCommitNotifications() {

        this.connectionWithCommitsDTO = CommitUtils.connectionWithCommitsDTO;

        if(this.connectionWithCommitsDTO.getId() != null) {

            logger.info("Scheduled call to Gitlab API - connectionWithCommitsDTO is NOT null");

            Connection connection = connectionService.getConnectionById(Long.parseLong(this.connectionWithCommitsDTO.getId()));

            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", "application/json");

            if (connection.getToken() != null && !connection.getToken().equals("")) {
                headers.setBearerAuth(connection.getToken());
            }

            String url = UrlUtils.PROJECT_URL_PREFIX + connection.getProjectId() +
                    UrlUtils.PROJECT_COMMITS_SUFFIX + connection.getBranchName();

            HttpEntity entity = new HttpEntity(headers);

            try {
                ResponseEntity<CommitDTO[]> res = myRestTemplate.exchange(url, HttpMethod.GET, entity, CommitDTO[].class, 1);
                this.connectionWithCommitsDTO = CommitUtils.commitListMapping(Objects.requireNonNull(res.getBody()), ConnectionUtils.connectionToDTO(connection));

                this.applicationEventPublisher.publishEvent(this.connectionWithCommitsDTO);
                logger.info("Published event: " + this.connectionWithCommitsDTO.getName() +
                        " " + this.connectionWithCommitsDTO.getUrl());
            } catch (HttpClientErrorException e) {
                logger.error("Rest template gitlab API call COMMITS ERROR: " + e.getMessage() + ".");
            }

        }
    }

    @Scheduled(fixedDelay = 15000)
    public void publishPullRequestNotifications() {

        this.connectionsWithPullRequestsDTO = PullRequestUtils.connectionsWithPullRequestsDTO;

        if(this.connectionsWithPullRequestsDTO.getId() != null) {


            logger.info("Scheduled call to Gitlab API - connectionsWithPullRequestsDTO is NOT null");

            Connection connection = connectionService.getConnectionById(Long.parseLong(this.connectionsWithPullRequestsDTO.getId()));

            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", "application/json");

            if (connection.getToken() != null && !connection.getToken().equals("")) {
                headers.setBearerAuth(connection.getToken());
            }

            String url = UrlUtils.PROJECT_URL_PREFIX + connection.getProjectId() +
                    UrlUtils.PROJECT_PULL_REQUESTS_SUFFIX + connection.getBranchName();

            HttpEntity entity = new HttpEntity(headers);

            try {
                ResponseEntity<PullRequestDTO[]> res = myRestTemplate.exchange(url, HttpMethod.GET, entity, PullRequestDTO[].class, 1);
                this.connectionsWithPullRequestsDTO = PullRequestUtils.pullRequestListMapping(Objects.requireNonNull(res.getBody()), ConnectionUtils.connectionToDTO(connection));

                this.applicationEventPublisher.publishEvent(this.connectionsWithPullRequestsDTO);
                logger.info("Published event: " + this.connectionsWithPullRequestsDTO.getName() +
                        " " + this.connectionsWithPullRequestsDTO.getUrl());
            } catch (HttpClientErrorException e) {
                logger.error("Rest template gitlab API call PULL REQUESTS ERROR: " + e.getMessage() + ".");
            }

        }
    }

}
