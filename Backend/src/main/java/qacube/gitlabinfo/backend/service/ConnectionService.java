package qacube.gitlabinfo.backend.service;

import org.springframework.stereotype.Service;
import qacube.gitlabinfo.backend.DTO.ApplicationWithConnectionsDTO;
import qacube.gitlabinfo.backend.DTO.ConnectionDTO;
import qacube.gitlabinfo.backend.DTO.ConnectionWithCommitsDTO;
import qacube.gitlabinfo.backend.DTO.ConnectionsWithPullRequestsDTO;
import qacube.gitlabinfo.backend.model.Connection;

import java.util.List;

@Service
public interface ConnectionService {

    List<ConnectionDTO> getAll();
    List<ConnectionDTO> getConnectionsByApplicationId(Long id);
    ApplicationWithConnectionsDTO getApplicationWithConnections(Long id);
    boolean deleteConnection(ConnectionDTO connectionDTO);
    ConnectionDTO createConnection(ConnectionDTO connectionDTO);
    ConnectionDTO updateConnection(ConnectionDTO connectionDTO);
    ConnectionWithCommitsDTO getConnectionCommits(Long id);
    ConnectionsWithPullRequestsDTO getConnectionPullRequests(Long id);
    Connection getConnectionById(Long id);
}
