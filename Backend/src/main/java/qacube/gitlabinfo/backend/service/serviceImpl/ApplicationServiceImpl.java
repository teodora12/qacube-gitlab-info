package qacube.gitlabinfo.backend.service.serviceImpl;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qacube.gitlabinfo.backend.DTO.ApplicationDTO;
import qacube.gitlabinfo.backend.DTO.ConnectionDTO;
import qacube.gitlabinfo.backend.model.Application;
import qacube.gitlabinfo.backend.model.Connection;
import qacube.gitlabinfo.backend.model.utils.ApplicationUtils;
import qacube.gitlabinfo.backend.model.utils.ConnectionUtils;
import qacube.gitlabinfo.backend.repository.ApplicationRepository;
import qacube.gitlabinfo.backend.service.ApplicationService;
import qacube.gitlabinfo.backend.service.ConnectionService;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ApplicationServiceImpl implements ApplicationService {

    private static Logger logger = LoggerFactory.getLogger(ApplicationServiceImpl.class);

    private ApplicationRepository applicationRepository;

    @Autowired
    private ConnectionService connectionService;

    public ApplicationServiceImpl(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }


    @Override
    public List<ApplicationDTO> getAll() {

        List<ApplicationDTO> applicationDTOS = new ArrayList<>();
        List<Application> applications = this.applicationRepository.findAll();

        for(Application a: applications){
            ApplicationDTO applicationDTO = ApplicationUtils.applicationToDTO(a);
            applicationDTOS.add(applicationDTO);
        }

        return applicationDTOS;
    }

    @Override
    public Application getApplicationById(Long id) {
        return this.applicationRepository.findApplicationById(id);
    }

    @Override
    public ApplicationDTO createApplication(ApplicationDTO applicationDTO) {

        ApplicationDTO returnApplicationDTO = new ApplicationDTO();

        if(applicationDTO.getId() != null && !applicationDTO.getId().equals("")){
            Application application = applicationRepository.findApplicationById(Long.parseLong(applicationDTO.getId()));
            if(application != null){
                logger.error("Application do not exist. applicationRepository.findApplicationById(id) resulted with value of null.");
                return null;
            }
        }else if (!applicationDTO.getName().equals("") && applicationDTO.getName() != null){  //ne valja uslov
            Application application = applicationRepository.findApplicationByName(applicationDTO.getName());
            if(application != null){
                returnApplicationDTO.setName("null");
                logger.error("Application with given name already exists.");
                return returnApplicationDTO;
            } else {
                Application newApplication = new Application();
                newApplication.setName(applicationDTO.getName());
                applicationRepository.save(newApplication);
                logger.info("Application: " + newApplication.getName() + ", ID: " + newApplication.getId() + " is created and stored into database.");
                returnApplicationDTO = ApplicationUtils.applicationToDTO(newApplication);
                return returnApplicationDTO;
            }
        }
        returnApplicationDTO.setName("empty");
        return returnApplicationDTO;

    }

    @Override
    public ApplicationDTO updateApplication(ApplicationDTO applicationDTO) {
        if(applicationDTO.getId() != null){

            Application application = applicationRepository.findApplicationById(Long.parseLong(applicationDTO.getId()));

            if(application != null){

                if(application.getId().equals(Long.parseLong(applicationDTO.getId()))) {
                    application.setName(applicationDTO.getName());
                    application.setId(Long.parseLong(applicationDTO.getId()));
                    this.applicationRepository.save(application);
                    logger.info("Application: " + application.getName() + ", ID: " + application.getId() + " is updated and stored into database.");
                    return applicationDTO;
                }
            }
            logger.error("Application do not exist. applicationRepository.findApplicationById(id) resulted with value of null.");
            return null;
        }
        logger.error("applicationDTO.getId() resulted with value of null.");
        return null;
    }

    @Override
    public boolean deleteApplication(Long id) {
        Application application = this.applicationRepository.findApplicationById(id);
        try{
            List<ConnectionDTO> connectionDTOS = this.connectionService.getConnectionsByApplicationId(id);
            if(connectionDTOS.isEmpty()){
                this.applicationRepository.delete(application);
                logger.info("Application: " + application.getName() + ", ID: " + application.getId() + " has been removed from database.");
                return true;
            } else {
                for (ConnectionDTO connectionDTO : connectionDTOS) {
                    this.connectionService.deleteConnection(connectionDTO);
                    logger.info("Connection: " + connectionDTO.getName() + ", ID: " + connectionDTO.getId() + " has been removed from database.");
                }

                try {
                    this.applicationRepository.delete(application);
                    logger.info("Application: " + application.getName() + ", ID: " + application.getId() + " has been removed from database.");
                } catch (Exception e){
                    logger.error("Cannot remove application: " + application.getName() + ", ID: " + application.getId() + " from database." +
                            "ERROR: " + e.getMessage() + ".");
                    return false;
                }

                return true;
            }

        } catch (Exception e){
            logger.error("Cannot remove application with ID: " + id + " from database. ERROR: " + e.getMessage() + ".");
            return false;
        }
    }

    @Override
    public Application getApplicationByName(String name) {
        return this.applicationRepository.findApplicationByName(name);
    }

    @Override
    public Application saveApplication(Application application) {
        return applicationRepository.save(application);
    }
}
