package qacube.gitlabinfo.backend.service;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import qacube.gitlabinfo.backend.service.dataFetcher.ApplicationDataFetcher;
import qacube.gitlabinfo.backend.service.dataFetcher.ApplicationsDataFetcher;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URL;

@Service
public class GraphQLService {


    private GraphQL graphQL;

    @Autowired
    private ApplicationDataFetcher applicationDataFetcher;

    @Autowired
    private ApplicationsDataFetcher applicationsDataFetcher;

    @PostConstruct
    public void loadSchema() throws IOException {


        URL url = Resources.getResource(GraphQL.class, "schema.graphqls");
        String sdl = Resources.toString(url, Charsets.UTF_8);

        TypeDefinitionRegistry typeDefinitionRegistry = new SchemaParser().parse(sdl);
        RuntimeWiring wiring = buildRuntimeWiring();
        GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(typeDefinitionRegistry, wiring);
        graphQL = GraphQL.newGraphQL(schema).build();
    }

    private RuntimeWiring buildRuntimeWiring() {

        return RuntimeWiring.newRuntimeWiring()
                .type("Query", typeWiring ->  typeWiring
                        .dataFetcher("getApplicationWithConnections", applicationDataFetcher)
                        .dataFetcher("getApplications", applicationsDataFetcher))
                .build();

    }

    @Bean
    public GraphQL getGraphQL() {
        return graphQL;
    }

}
