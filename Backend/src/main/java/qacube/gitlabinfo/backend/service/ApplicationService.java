package qacube.gitlabinfo.backend.service;

import org.springframework.stereotype.Service;
import qacube.gitlabinfo.backend.DTO.ApplicationDTO;
import qacube.gitlabinfo.backend.DTO.ApplicationWithConnectionsDTO;
import qacube.gitlabinfo.backend.model.Application;

import java.util.List;
import java.util.Set;

@Service
public interface ApplicationService {

    List<ApplicationDTO> getAll();
    Application getApplicationById(Long id);
    ApplicationDTO createApplication(ApplicationDTO application);
    ApplicationDTO updateApplication(ApplicationDTO application);
    boolean deleteApplication(Long id);
    Application getApplicationByName(String name);
    Application saveApplication(Application application);

}
