package qacube.gitlabinfo.backend.DTO;

import lombok.Data;

@Data
public class ProjectDTO {

    private Long id;

    private String description;

    private String name;

    private String web_url;

    private NamespaceDTO namespace;

}
