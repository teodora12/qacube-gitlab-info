package qacube.gitlabinfo.backend.DTO;

import lombok.Data;

import java.util.List;

@Data
public class CommitDTO {

    private String id;

    private String short_id;

    private String created_at;

    private String message;

    private String author_name;

    private String author_email;

    private String authored_date;

    private String committer_name;

    private String committer_email;

    private String committed_date;

    private String web_url;



    public CommitDTO() {
    }
}
