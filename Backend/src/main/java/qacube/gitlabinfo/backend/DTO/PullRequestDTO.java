package qacube.gitlabinfo.backend.DTO;

import lombok.Data;

@Data
public class PullRequestDTO {

    private Long id;

    private String title;

    private String created_at;

    private String updated_at;

    private String merged_at;

    private String target_branch;

    private String source_branch;

    private UserDTO author;

    private UserDTO assignee;


    public PullRequestDTO() {
    }
}
