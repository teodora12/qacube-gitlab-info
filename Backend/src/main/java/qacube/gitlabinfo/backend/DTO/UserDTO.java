package qacube.gitlabinfo.backend.DTO;

import lombok.Data;

@Data
public class UserDTO {

    private Long id;

    private String name;

    private String username;

}
