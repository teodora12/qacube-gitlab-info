package qacube.gitlabinfo.backend.DTO;

import lombok.Data;

@Data
public class NamespaceDTO {

    private Long id;

    private String name;

    private String path;

    private String web_url;

}
