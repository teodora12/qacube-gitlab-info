package qacube.gitlabinfo.backend.DTO;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ConnectionDTO {

    private String id;

    @NotBlank
    private String name;

    @NotBlank
    private String url;

    private String token;

    @NotNull
    private ApplicationDTO applicationDTO;

    @NotBlank
    public String projectName;

    @NotBlank
    private String branchName;

    private String projectId;


    public ConnectionDTO() {
    }
}
