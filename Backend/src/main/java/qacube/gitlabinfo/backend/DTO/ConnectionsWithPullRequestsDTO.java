package qacube.gitlabinfo.backend.DTO;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ConnectionsWithPullRequestsDTO {

    private String id;

    private String name;

    private String url;

    private String token;

    private ApplicationDTO applicationDTO;

    public String projectName;

    private String branchName;

    private List<PullRequestDTO> pullRequestDTOS;

    private String project_id;


    public ConnectionsWithPullRequestsDTO() {
        pullRequestDTOS = new ArrayList<>();
    }
}
