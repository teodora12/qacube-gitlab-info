package qacube.gitlabinfo.backend.DTO;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;


@Data
public class ApplicationDTO {

    private String id;

    @NotBlank
    private String name;

    public ApplicationDTO() {
    }
}
