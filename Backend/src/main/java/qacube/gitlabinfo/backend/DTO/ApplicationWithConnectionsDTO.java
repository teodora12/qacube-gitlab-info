package qacube.gitlabinfo.backend.DTO;

import lombok.Data;


import java.util.ArrayList;
import java.util.List;

@Data
public class ApplicationWithConnectionsDTO {

    private ApplicationDTO applicationDTO;

    private List<ConnectionDTO> connectionDTOS;

    public ApplicationWithConnectionsDTO() {

        applicationDTO = new ApplicationDTO();
        connectionDTOS = new ArrayList<>();
    }
}
