package qacube.gitlabinfo.backend.model.utils;


import lombok.Data;
import qacube.gitlabinfo.backend.DTO.CommitDTO;
import qacube.gitlabinfo.backend.DTO.ConnectionDTO;
import qacube.gitlabinfo.backend.DTO.ConnectionWithCommitsDTO;

public class CommitUtils {

    public static ConnectionWithCommitsDTO connectionWithCommitsDTO = new ConnectionWithCommitsDTO();

    public static ConnectionWithCommitsDTO commitListMapping(CommitDTO[] commitDTOS, ConnectionDTO connectionDTO){

        ConnectionWithCommitsDTO connectionWithCommitsDTO = new ConnectionWithCommitsDTO();

        connectionWithCommitsDTO.setId(connectionDTO.getId());
        connectionWithCommitsDTO.setName(connectionDTO.getName());
        connectionWithCommitsDTO.setBranchName(connectionDTO.getBranchName());
        connectionWithCommitsDTO.setProjectName(connectionDTO.getProjectName());
        connectionWithCommitsDTO.setApplicationDTO(connectionDTO.getApplicationDTO());
        connectionWithCommitsDTO.setToken(connectionDTO.getToken());
        connectionWithCommitsDTO.setUrl(connectionDTO.getUrl());
        connectionWithCommitsDTO.setProject_id(connectionDTO.getProjectId());

        int arraySize = commitDTOS.length;

        if(arraySize >= 10) {
            for (int i = 0; i < 10; i++) {

                commitDTOS[i].setCommitted_date(DateUtils.parseDate(commitDTOS[i].getCommitted_date()));
                connectionWithCommitsDTO.getCommitDTOS().add(commitDTOS[i]);
            }
        } else {
            for (int i = 0; i < arraySize; i++) {
                commitDTOS[i].setCommitted_date(DateUtils.parseDate(commitDTOS[i].getCommitted_date()));
                connectionWithCommitsDTO.getCommitDTOS().add(commitDTOS[i]);
            }
        }


        return connectionWithCommitsDTO;


    }



}
