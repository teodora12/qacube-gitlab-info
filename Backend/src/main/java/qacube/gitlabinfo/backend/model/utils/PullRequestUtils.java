package qacube.gitlabinfo.backend.model.utils;

import qacube.gitlabinfo.backend.DTO.ConnectionDTO;
import qacube.gitlabinfo.backend.DTO.ConnectionsWithPullRequestsDTO;
import qacube.gitlabinfo.backend.DTO.PullRequestDTO;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PullRequestUtils {

    public static Pattern p = Pattern.compile("((^| )[A-Za-z])");

    public static ConnectionsWithPullRequestsDTO connectionsWithPullRequestsDTO = new ConnectionsWithPullRequestsDTO();


    public static ConnectionsWithPullRequestsDTO pullRequestListMapping(PullRequestDTO[] pullRequestDTOS, ConnectionDTO connectionDTO){

        ConnectionsWithPullRequestsDTO connectionsWithPullRequestsDTO = new ConnectionsWithPullRequestsDTO();

        connectionsWithPullRequestsDTO.setId(connectionDTO.getId());
        connectionsWithPullRequestsDTO.setName(connectionDTO.getName());
        connectionsWithPullRequestsDTO.setBranchName(connectionDTO.getBranchName());
        connectionsWithPullRequestsDTO.setProjectName(connectionDTO.getProjectName());
        connectionsWithPullRequestsDTO.setApplicationDTO(connectionDTO.getApplicationDTO());
        connectionsWithPullRequestsDTO.setToken(connectionDTO.getToken());
        connectionsWithPullRequestsDTO.setUrl(connectionDTO.getUrl());
        connectionsWithPullRequestsDTO.setProject_id(connectionDTO.getProjectId());


        int arraySize = pullRequestDTOS.length;

        if(arraySize >= 10) {
            for (int i = 0; i < 10; i++) {

                pullRequestDTOS[i].setCreated_at(DateUtils.parseDate(pullRequestDTOS[i].getCreated_at()));

                if(pullRequestDTOS[i].getAssignee() != null) {
                    pullRequestDTOS[i].getAssignee().setName(PullRequestUtils.makeInitials(pullRequestDTOS[i].getAssignee().getName()));
                }
                if(pullRequestDTOS[i].getAuthor() != null) {
                    pullRequestDTOS[i].getAuthor().setName(PullRequestUtils.makeInitials(pullRequestDTOS[i].getAuthor().getName()));
                }

                connectionsWithPullRequestsDTO.getPullRequestDTOS().add(pullRequestDTOS[i]);
            }
        } else {
            for (int i = 0; i < arraySize; i++) {

                pullRequestDTOS[i].setCreated_at(DateUtils.parseDate(pullRequestDTOS[i].getCreated_at()));

                if(pullRequestDTOS[i].getAssignee() != null) {
                    pullRequestDTOS[i].getAssignee().setName(PullRequestUtils.makeInitials(pullRequestDTOS[i].getAssignee().getName()));
                }
                if(pullRequestDTOS[i].getAuthor() != null) {
                    pullRequestDTOS[i].getAuthor().setName(PullRequestUtils.makeInitials(pullRequestDTOS[i].getAuthor().getName()));
                }
                connectionsWithPullRequestsDTO.getPullRequestDTOS().add(pullRequestDTOS[i]);
            }
        }

        return connectionsWithPullRequestsDTO;

    }

    public static String makeInitials(String fullName) {
        Matcher m = p.matcher(fullName);
        String initials = "";
        while (m.find()) {
            initials += m.group().trim() + ". ";
        }

        return initials.toUpperCase();
    }

}
