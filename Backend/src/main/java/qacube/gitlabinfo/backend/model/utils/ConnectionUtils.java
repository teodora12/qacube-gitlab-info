package qacube.gitlabinfo.backend.model.utils;

import qacube.gitlabinfo.backend.DTO.ConnectionDTO;
import qacube.gitlabinfo.backend.model.Connection;

public class ConnectionUtils {


    public static ConnectionDTO connectionToDTO(Connection connection){

        ConnectionDTO connectionDTO = new ConnectionDTO();

        connectionDTO.setId(String.valueOf(connection.getId()));
        connectionDTO.setName(connection.getName());
        connectionDTO.setProjectName(connection.getProjectName());
        connectionDTO.setUrl(connection.getUrl());
        connectionDTO.setBranchName(connection.getBranchName());
        connectionDTO.setApplicationDTO(ApplicationUtils.applicationToDTO(connection.getApplication()));

        if(connection.getToken() != null && !connection.getToken().equals("")) {
            connectionDTO.setToken(connection.getToken());
        }
        if(connection.getProjectId() != null){
            connectionDTO.setProjectId(String.valueOf(connection.getProjectId()));
        }

        return connectionDTO;
    }

    public static Connection connectionDTOToConnection(ConnectionDTO connectionDTO){

        Connection connection = new Connection();

        if(!connectionDTO.getId().equals("") && connectionDTO.getId() != null) {
            connection.setId(Long.parseLong(connectionDTO.getId()));
        }
        connection.setName(connectionDTO.getName());
        connection.setBranchName(connectionDTO.getBranchName());
        connection.setUrl(connectionDTO.getUrl());
        connection.setProjectName(connectionDTO.getProjectName());
        connection.setToken(connectionDTO.getToken());
        connection.setApplication(ApplicationUtils.applicationDTOToApplication(connectionDTO.getApplicationDTO()));

        if(connectionDTO.getProjectId() != null && !connectionDTO.getProjectId().equals("") ){
            connection.setProjectId(Long.parseLong(connectionDTO.getProjectId()));
        }

        return connection;
    }

}
