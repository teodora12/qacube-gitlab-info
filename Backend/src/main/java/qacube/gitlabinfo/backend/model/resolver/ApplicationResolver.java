package qacube.gitlabinfo.backend.model.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qacube.gitlabinfo.backend.model.Application;
import qacube.gitlabinfo.backend.service.ApplicationService;
import qacube.gitlabinfo.backend.service.ConnectionService;

@Component
public class ApplicationResolver implements GraphQLResolver<Application> {

    @Autowired
    private ConnectionService connectionService;

}
