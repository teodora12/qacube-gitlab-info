package qacube.gitlabinfo.backend.model.utils;

public class UrlUtils {

    public static String PROJECTS_URL_GITLAB = "https://gitlab.com/api/v4/projects?search=";
    public static String PROJECT_URL_PREFIX = "https://gitlab.com/api/v4/projects/";
    public static String PROJECT_COMMITS_SUFFIX = "/repository/commits?ref_name=";
    public static String PROJECT_PULL_REQUESTS_SUFFIX = "/merge_requests?target_branch=";

}
