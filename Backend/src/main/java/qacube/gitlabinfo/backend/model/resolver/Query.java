package qacube.gitlabinfo.backend.model.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qacube.gitlabinfo.backend.DTO.ApplicationDTO;
import qacube.gitlabinfo.backend.DTO.ApplicationWithConnectionsDTO;
import qacube.gitlabinfo.backend.model.Application;
import qacube.gitlabinfo.backend.model.Connection;
import qacube.gitlabinfo.backend.repository.ApplicationRepository;
import qacube.gitlabinfo.backend.repository.ConnectionRepository;
import qacube.gitlabinfo.backend.service.ApplicationService;
import qacube.gitlabinfo.backend.service.ConnectionService;

@Component
public class Query implements GraphQLQueryResolver {

    @Autowired
    private ConnectionService connectionService;

    @Autowired
    private ApplicationService applicationService;


    public Iterable<ApplicationDTO> getApplications() {
        return applicationService.getAll();
    }

    public ApplicationWithConnectionsDTO getApplicationWithConnections(Long id){
        return connectionService.getApplicationWithConnections(id);
    }

}
