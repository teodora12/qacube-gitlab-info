package qacube.gitlabinfo.backend.model;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Connection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String name;

    @Column
    private String url;

    @Column
    private String token;

    @ManyToOne
    private Application application;

    @Column
    public String projectName;

    @Column
    public Long projectId;

    @Column
    private String branchName;


    public Connection() {
    }
}
