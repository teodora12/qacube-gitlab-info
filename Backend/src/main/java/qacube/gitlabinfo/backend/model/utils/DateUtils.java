package qacube.gitlabinfo.backend.model.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    public static String parseDate(String date){

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        try {
            Date dateUtil = formatter.parse(date);
            String dateResult = formatter2.format(dateUtil);
            return dateResult;
        } catch (Exception e){
            e.printStackTrace();
        }

        return date;

    }

}
