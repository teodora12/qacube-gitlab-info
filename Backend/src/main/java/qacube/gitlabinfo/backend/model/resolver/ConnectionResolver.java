package qacube.gitlabinfo.backend.model.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qacube.gitlabinfo.backend.DTO.ApplicationWithConnectionsDTO;
import qacube.gitlabinfo.backend.model.Application;
import qacube.gitlabinfo.backend.model.Connection;
import qacube.gitlabinfo.backend.repository.ApplicationRepository;
import qacube.gitlabinfo.backend.service.ApplicationService;
import qacube.gitlabinfo.backend.service.ConnectionService;

@Component
public class ConnectionResolver implements GraphQLResolver<Connection> {

    @Autowired
    private ApplicationService applicationService;

    public Application getApplication(Connection connection){
        return applicationService.getApplicationById(connection.getApplication().getId());
    }

}
