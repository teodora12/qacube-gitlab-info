package qacube.gitlabinfo.backend.model.utils;

import qacube.gitlabinfo.backend.DTO.ApplicationDTO;
import qacube.gitlabinfo.backend.model.Application;

public class ApplicationUtils {

    public static ApplicationDTO applicationToDTO(Application application){

        ApplicationDTO applicationDTO = new ApplicationDTO();

        applicationDTO.setName(application.getName());
        applicationDTO.setId(String.valueOf(application.getId()));

        return applicationDTO;
    }

    public static Application applicationDTOToApplication(ApplicationDTO applicationDTO){

        Application application = new Application();

        if(application.getId() != null && !String.valueOf(application.getId()).equals("")) {
            application.setId(Long.parseLong(applicationDTO.getId()));
        }
        application.setName(applicationDTO.getName());

        return application;
    }
}
